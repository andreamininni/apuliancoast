<?php

use App\Location;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('province_id');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->string('name');
            $table->tinyInteger('traffic')->nullable();
            $table->timestamps();
        });

        $bari = [
            "Bari", "Polignano", "Monopoli", "Molfetta", "Giovinazzo"
        ];

        $lecce = [
            "Lecce", "San Cataldo", "Torre dell'Orso", "Otranto", "Santa Maria di Leuca" 
        ];

        $brindisi = [
            "Brindisi", "Specchiolla", "Torre San Gennaro", "Savelletri", "Posticeddu" 
        ];

        $taranto = [
            "Taranto", "Castellaneta Marina", "Pulsano", "San Vito", "Praia a mare" 
        ];

        $foggia = [
            "Pugnochiuso", "San Menaio", "Rodi Garganico", "Vieste", "Peschici" 
        ];

        foreach ($bari as $location) {
            $beach1 = new Location();
            $beach1->name = $location;
            $beach1->province_id = 1;
            $beach1->save();
        }

        foreach ($lecce as $location) {
            $beach1 = new Location();
            $beach1->name = $location;
            $beach1->province_id = 3;
            $beach1->save();
        }

        foreach ($brindisi as $location) {
            $beach1 = new Location();
            $beach1->name = $location;
            $beach1->province_id = 2;
            $beach1->save();
        }

        foreach ($taranto as $location) {
            $beach1 = new Location();
            $beach1->name = $location;
            $beach1->province_id = 4;
            $beach1->save();
        }

        foreach ($foggia as $location) {
            $beach1 = new Location();
            $beach1->name = $location;
            $beach1->province_id = 5;
            $beach1->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
