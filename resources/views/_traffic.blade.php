@switch($traffic)
    @case(1)
        <h4 class="text-success font-weight-bold">Scorrevole</h4>
        @break
    @case(2)
        <h4 class="text-success font-weight-bold">Abbastanza scorrevole</h4>
        @break
    @case(3)
        <h4 class="text-warning font-weight-bold">Poco scorrevole</h4>
        @break
    @case(4)
        <h4 class="text-danger font-weight-bold">Intenso</h4>
        @break
    @case(5)
        <h4 class="text-danger font-weight-bold">Molto intenso</h4>
        @break
    @default
        
@endswitch