
<div id="searchResults">
  <div class="row text-center">
  @foreach ($locations as $location)
    <div class="col-12 col-md-4 my-5">
        <div class="card">
            <img src="/img/sabbiadoro-lido-visto.jpg" class="card-img-top" alt="...">
            <div class="card-body text-left">
                <h5 class="card-title font-weight-bold">{{$location->name}}</h5>
                <p class="card-text">Provincia: {{$location->province->name}}</p>
                <h4>Traffico: </h4>@include('_traffic', ['traffic' => $location->traffic])
            </div>
        </div>
    </div>
  @endforeach
  </div>
</div>

