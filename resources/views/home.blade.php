@extends('layouts.app')

@section('content')

<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-12 text-center">
        <h1 class="font-weight-light">Benvenuto in Apulian Coast</h1>
        <p class="lead">Qui puoi monitorare l'andamento del traffico vicino le tue spiagge preferite</p>
      </div>
    </div>
  </div>
</header>
  
<div class="container">
  <div class="row text-center">
    <div class="col-12">
      <button class="btn btn-info" id="btn-all">Tutte</button>
      @foreach ($provinces as $province)
        <button class="btn btn-info" btn-id="{{$province->id}}">{{$province->name}}</button>
      @endforeach
    </div>
  </div>
  <div id="searchResults">
    <div class="row text-center">
    @foreach ($locations as $location)
      <div class="col-12 col-md-4 my-5">
          <div class="card">
              <img src="/img/sabbiadoro-lido-visto.jpg" class="card-img-top" alt="...">
              <div class="card-body text-left">
                  <h5 class="card-title font-weight-bold">{{$location->name}}</h5>
                  <p class="card-text">Provincia: {{$location->province->name}}</p>
                  <h4>Traffico: </h4>@include('_traffic', ['traffic' => $location->traffic])
              </div>
          </div>
      </div>
    @endforeach
    </div>
  </div>
</div>
@endsection
