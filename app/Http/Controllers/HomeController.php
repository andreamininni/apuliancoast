<?php

namespace App\Http\Controllers;

use App\Beach;
use App\Location;
use App\Province;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $provinces = Province::all();
     
        $locations = Location::all();
        foreach ($locations as $location) {
            $location->setTraffic();
            $location->save();
        }

        $locations = Location::orderBy('traffic')->get();
        

        return view('home', compact('locations', 'provinces'));
    }

    public function getLocationByProvince($id)
    {
        
        $locations = Location::all();
        foreach ($locations as $location) {
            $location->setTraffic();
            $location->save();
        }
        $locations = Location::where('province_id', $id)->orderBy('traffic')->get();
        
        return view('_searchResult', compact('locations'));
    }

    public function getAllLocations()
    {
        $locations = Location::all();
        foreach ($locations as $location) {
            $location->setTraffic();
            $location->save();
        }

        $locations = Location::orderBy('traffic')->get();   

        return view('_searchResult', compact('locations'));
        
    }
}
