<?php

namespace App;

use App\Location;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{

     
    public function locations()
    {
        return $this->hasMany(Location::class);
    }
}
