<?php

namespace App;

use App\Province;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
     protected $fillable = [
        
       "name", "city"    
        
    ];

    public function setTraffic()
    {
        $this->traffic = rand(1,5);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
